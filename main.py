import pathlib
import argparse
import os
import time

os.environ['PYTHONHASHSEED'] = '0'

from matplotlib import pyplot
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import backend as K
from tensorflow.keras import layers
from tensorflow.keras.applications.vgg16 import VGG16
from my_types import restricted_int, positive_float


def get_data_paths(main_dir):
    train_dir = main_dir.joinpath('train')
    validation_dir = main_dir.joinpath('validation')
    test_dir = main_dir.joinpath('test')
    return train_dir, validation_dir, test_dir


def create_data_flows(dataset_path, image_size, batch_size):
    train_dir, validation_dir, test_dir = get_data_paths(dataset_path)

    generator = ImageDataGenerator(rescale=1.0 / 255.0)
    # augmentation
    train_gen = ImageDataGenerator(rescale=1.0 / 255.0,
                                   shear_range=0.1,
                                   zoom_range=0.1,
                                   width_shift_range=0.1,
                                   height_shift_range=0.1,
                                   horizontal_flip=True)

    train_flow = train_gen.flow_from_directory(
        train_dir,
        target_size=image_size,
        batch_size=batch_size,
        class_mode='binary')
    validation_flow = generator.flow_from_directory(
        validation_dir,
        target_size=image_size,
        batch_size=batch_size,
        class_mode='binary')
    test_flow = generator.flow_from_directory(
        test_dir,
        target_size=image_size,
        batch_size=1,
        class_mode='binary',
        shuffle=False)
    return train_flow, validation_flow, test_flow


def define_model_vgg1(input_shape, learning_rate):
    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    model = Sequential([
        layers.Conv2D(32, 3, padding='same', activation='relu', input_shape=input_shape),
        layers.MaxPooling2D(2),

        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(1, activation='sigmoid')
    ])
    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['binary_accuracy'])
    return model


def define_model_vgg3(input_shape, learning_rate):
    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    model = Sequential([
        layers.Conv2D(32, 3, padding='same', activation='relu', input_shape=input_shape),
        layers.MaxPooling2D(2),

        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(2),

        layers.Conv2D(128, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(2),

        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dense(1, activation='sigmoid')
    ])
    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['binary_accuracy'])
    return model


def define_model_vgg3_gen(input_shape, learning_rate):
    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    model = Sequential([
        layers.Conv2D(32, 3, padding='same', activation='relu', input_shape=input_shape),
        layers.MaxPooling2D(2),
        layers.Dropout(0.1),

        layers.Conv2D(64, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.1),

        layers.Conv2D(128, 3, padding='same', activation='relu'),
        layers.MaxPooling2D(2),
        layers.Dropout(0.1),

        layers.Flatten(),
        layers.Dense(128, activation='relu'),
        layers.Dropout(0.5),
        layers.Dense(1, activation='sigmoid')
    ])
    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['binary_accuracy'])
    return model


def define_model_vgg16(input_shape, learning_rate):
    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    model = VGG16(include_top=False, input_shape=input_shape)
    for layer in model.layers:
        layer.trainable = False
    # add classifier layers
    flat1 = layers.Flatten()(model.layers[-1].output)
    class1 = layers.Dense(128, activation='relu')(flat1)
    output = layers.Dense(1, activation='sigmoid')(class1)

    model = tf.keras.Model(inputs=model.inputs, outputs=output)
    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['binary_accuracy'])
    return model


def summarize_diagnostics(history, model_type):
    pyplot.figure(figsize=(16, 9))
    pyplot.subplot(211)
    pyplot.title('Функция потерь (cross entropy)')
    pyplot.plot(history.history['loss'], color='blue', label='train')
    pyplot.plot(history.history['val_loss'], color='orange', label='test')
    pyplot.legend(loc='upper right')
    pyplot.xlabel("Эпоха")
    pyplot.ylabel("Потери")

    pyplot.subplot(212)
    pyplot.title('Точность классификации')
    pyplot.plot(history.history['binary_accuracy'], color='blue', label='train')
    pyplot.plot(history.history['val_binary_accuracy'], color='orange', label='test')
    pyplot.legend(loc='upper left')
    pyplot.xlabel("Эпоха")
    pyplot.ylabel("Точность")
    pyplot.tight_layout()

    filename = time.strftime("%Y%m%d_%H%M%S") + '_plot_' + model_type + '.png'
    pyplot.savefig(filename)
    pyplot.close()
    print('Кривые обучения сохранены в файл ' + filename)


def main(model_type, epochs, batch_size, learning_rate, verbose, num_cores):
    dataset_path = pathlib.Path("cats_vs_dogs")

    img_width, img_height = 224, 224
    image_size = (img_width, img_height)
    if K.image_data_format() == 'channels_first':
        input_shape = (3, img_width, img_height)
    else:
        input_shape = (img_width, img_height, 3)

    map_models = {
        'VGG1': define_model_vgg1(input_shape, learning_rate),
        'VGG3': define_model_vgg3(input_shape, learning_rate),
        'VGG3_D': define_model_vgg3_gen(input_shape, learning_rate),
        'VGG16': define_model_vgg16(input_shape, learning_rate),
    }

    train_flow, validation_flow, test_flow = create_data_flows(dataset_path, image_size, batch_size)
    model = map_models[model_type]
    history = model.fit(train_flow,
                        validation_data=validation_flow,
                        epochs=epochs,
                        workers=num_cores,
                        use_multiprocessing=True,
                        verbose=verbose)
    scores = model.evaluate(test_flow, verbose=verbose)
    summarize_diagnostics(history, model_type + "_%.2f" % (scores[1] * 100))
    print("Точность модели на тестовых данных: %.2f%%" % (scores[1] * 100))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-t', '--model_type', choices=['VGG1', 'VGG3', 'VGG3_D', 'VGG16'],
                        help='Тип модели. (default: %(default)s)',
                        default='A')
    parser.add_argument('--epochs', type=restricted_int,
                        help='Число эпох обучения. (default: %(default)s)',
                        default=25)
    parser.add_argument('--batch_size', type=restricted_int,
                        help='Число объектов, представленных в одном мини-сете обучения. (default: %(default)s)',
                        default=32)
    parser.add_argument('--learning_rate', type=positive_float,
                        help='Скорость обучения. (default: %(default)s)',
                        default=0.001)
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Включить расширенный лог. (default: %(default)s)')
    parser.add_argument('--num_cores', type=restricted_int,
                        help='Число задействованных ядер CPU. (default: all CPU cores)',
                        default=os.cpu_count())
    args = parser.parse_args()
    print(args)
    main(args.model_type, args.epochs, args.batch_size, args.learning_rate, args.verbose, args.num_cores)
