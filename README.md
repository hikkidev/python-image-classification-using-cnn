# Обзор
Репозиторий содержит python реализоцию **Convolutional neural network** для бинарной классификации кошек или собак.

Для установки зависимостей выполните `pip install -r requirements.txt` в папке с проектом.


## Использование

Подробная информация о глобальных параметрах скрипта
1) Перейти в папку с проектом: `cd python-image-classification-using-cnn`
2) Выполнить: `python main.py --help`


Инструкция по запуску классификации
1) Перейти в папку с проектом: `cd python-image-classification-using-cnn`
2) Подробнее о параметрах скрипта: `python main.py svm --help`
3) Выполнить: `python main.py -v -t VGG3_D --learning_rate 1e-4 --epochs 30`


## Реализация

Класс реализующий [CNN:VGG](./main.py)
